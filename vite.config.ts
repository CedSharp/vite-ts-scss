import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
  build: { outDir: 'out' },
  resolve: { alias: [{ find: '@', replacement: resolve(__dirname, 'src') }] },
  server: {
    host: '0.0.0.0',
    port: 3000,
  },
  test: {
    globals: true,
    environment: 'jsdom',
    coverage: {
      all: true,
      reporter: ['text', 'json', 'html'],
      exclude: [
        '.husky', // Husky has nothing to do with codebase
        'vite.config.ts', // Configuration files shouldn't be covered
        'src/index.ts', // Index file does not implement features, ignored
      ],
    },
  },
});
