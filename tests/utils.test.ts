import { describe, expect, it } from 'vitest';
import * as utils from '../src/utils';

describe('Utilities', () => {
  it('should export "slugify"', () => 'slugify' in utils);
  it('should correctly slugify', () => {
    expect(utils.slugify('My amazing post')).eq('my-amazing-post');
  });
});
