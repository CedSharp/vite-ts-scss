/**
 * Converts something like "My amazing post" into "my-amazing-post"
 * @param text The text to be slugified
 */
export const slugify = (text: string): string =>
  text.replace(/\W/g, '-').replace(/-+/g, '-').toLowerCase();
