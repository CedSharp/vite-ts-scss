import { slugify } from './utils';

const title = 'My amazing post';
const slug = slugify(title);

const h1 = document.createElement('h1');
h1.innerText = title;

const p = document.createElement('p');
p.innerText = `Slugified version: ${slug}.`;

document.body.appendChild(h1);
document.body.appendChild(p);
